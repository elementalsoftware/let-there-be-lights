"use strict";

var self;
module.exports = self = {};

var saveMgr = require("./save-manager.js");
var sceneMgr = require("./scene-manager.js");
var animMgr = require("./animation-manager.js");
var animRunner = require("./animation-runner");

var componentMap = {
	"channel": {
		component: require("./level-manager.js"),
		handler: function(component, data){
			component.setLevels(data);
		},
		getData: function(component){
			return component.getLevels();
		}
	},
	"uipage": {
		component: require("./ui-manager.js"),
		handler: function(component, data){
			component.setPages(data);
		},
		getData: function(component){
			return component.getPages();
		}
	},
	"scene": {
		component: sceneMgr,
		handler: function(component, data){
			component.setScenes(data);
		},
		getData: function(component){
			return component.getScenes();
		}
	},
	"anim": {
		component: animMgr,
		handler: function(component, data){
			component.setAnimations(data);
		},
		getData: function(component){
			return component.getAnimations();
		}
	},
	"animrun": {
		component: animRunner,
		handler: function(component, data){

		},
		getData: function(component){
			return component.getAnimations();
		}
	}
};



self.bind = function(io){
	io.on("connection", function(socket){
		var ignoreUpdate = false;

		//necessary for scope management
		var bindComponent = function(componentId){
			var component = componentMap[componentId];
			handlerFuncs[componentId] = function(data){
				if(!ignoreUpdate){
					socket.emit(componentId+"update", data);
				}
			};
			socket.on(componentId+"update", function(data){
				ignoreUpdate = true;
				component.handler(component.component, data);
				ignoreUpdate = false;
			});
			socket.on("request"+componentId+"update", function(){
				socket.emit(componentId+"update", component.getData(component.component));
			});
			component.component.on(componentId+"update", handlerFuncs[componentId]);
		};

		var handlerFuncs = {};
		var componentId;
		for(componentId in componentMap){
			if(componentMap.hasOwnProperty(componentId)){
				bindComponent(componentId);
			}
		}



		socket.on("activatescene", function(){
			sceneMgr.activateScene.apply(sceneMgr, arguments);
		});
		socket.on("requestsceneupdate", function(){
			socket.emit("sceneactivate", sceneMgr.getActiveScene());
		});
		var sceneActivateFunc = function(){
			var args = Array.prototype.slice.call(arguments);
			args.unshift("sceneactivate");
			socket.emit.apply(socket, args);
		};
		sceneMgr.on("sceneactivate", sceneActivateFunc);

		socket.on("activateanim", animRunner.startAnimation);
		socket.on("cancelanim", animRunner.cancelAnimation);

		socket.on("saveshow", function(){
			saveMgr.save();
		});
		socket.on("loadshow", function(){
			saveMgr.load();
		});

		socket.on("disconnect", function(){
			for(componentId in componentMap){
				if(componentMap.hasOwnProperty(componentId)){
					componentMap[componentId].component.removeListener(componentId+"update", handlerFuncs[componentId]);
				}
			}
			sceneMgr.removeListener("sceneactivate", sceneActivateFunc);
		});
	});
};
