"use strict";

module.exports = {
	levelmgr: require("./level-manager"),
	animmgr: require("./animation-manager"),
	scenemgr: require("./scene-manager"),
	uimgr: require("./ui-manager"),
	transitionmgr: require("./transition-manager"),
	socketio: require("./socketio")
};

setTimeout(function(){
	var saveMgr = require("./save-manager");
	saveMgr.load();
});
