"use strict";

var fs = require("fs");

var filename = "save.json";

module.exports = {
	save: function(/*filename*/){
		var dmxMgr = require("./dmx-frontend.js");
		var saveData = {};
		for(var idx in dmxMgr){
			if(dmxMgr.hasOwnProperty(idx) && dmxMgr[idx].getSaveName){
				saveData[dmxMgr[idx].getSaveName()] = dmxMgr[idx].getSaveData();
			}
		}
		var outStr = JSON.stringify(saveData);
		fs.writeFile("saved-shows/"+filename, outStr);
	},
	load: function(/*filename*/){
		var dmxMgr = require("./dmx-frontend.js");
		fs.readFile("saved-shows/"+filename, function(err, fileData){
			if(err){ return; }
			var saveData = JSON.parse(fileData);
			if(saveData){
				for(var idx in dmxMgr){
					if(dmxMgr.hasOwnProperty(idx) && dmxMgr[idx].getSaveName && saveData.hasOwnProperty(dmxMgr[idx].getSaveName())){
						dmxMgr[idx].restoreSaveData(saveData[dmxMgr[idx].getSaveName()]);
					}
				}
			}
		});
	},
	getSaveFileList: function(){
		//TODO
	}
};

setTimeout(function(){module.exports.save()}, 10000);