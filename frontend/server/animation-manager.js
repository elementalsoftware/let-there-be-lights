"use strict";

var events = require("events");

var self;
module.exports = self = new events.EventEmitter();

// Animations
/* An animation has:
 - A unique ID (uuid)
 - A name,
 - A channel count (raw dmx)
 - A list of faders (for the edit UI, same structure)
 - A list of keyframes

 A keyframe ia the same as a scene, with an additional waitTime parameter
 indicating how long before the next keyframe activates
*/

var animations = {};


self.getSaveName = function(){
	return "anim-manager";
};

self.getSaveData = function(){
	return animations;
};

self.restoreSaveData = function(saveData){
	animations = saveData || {};
	this.emit("animupdate", animations);
};

self.getAnimations = function(animIds){
	if(typeof animIds === "string"){
		if(animations.hasOwnProperty(animIds)){
			return animations[animIds];
		}
		return;
	}
	if(typeof animIds === "undefined"){
		return animations;
	}
	var ret = {};
	animIds.forEach(function(idx){
		if(animations.hasOwnProperty(idx)){
			ret[idx] = animations[idx];
		}
	});
	return ret;
};

self.setAnimations = function(newAnims){
	var anim;
	var updateList = {};
	for(var animIdx in newAnims){
		if(newAnims.hasOwnProperty(animIdx)){
			if(newAnims[animIdx]){
				anim = newAnims[animIdx];
				animations[animIdx] = anim;
			}else{
				delete animations[animIdx];
				anim = undefined;
			}
			updateList[animIdx] = anim;
		}
	}
	if(Object.keys(updateList).length){
		this.emit("animupdate", updateList);
	}
};
