"use strict";

var events = require("events");

var self;
module.exports = self = new events.EventEmitter();

var pages = [
	{
		name: "Base",
		channels: []
	},
	{
		name: "Other Page",
		channels: [{
			name: "Lamp 4",
			channelId: 4,
			type: "basic"
		},{
			name: "Lamp 6",
			channelId: 6,
			type: "basic"
		},{
			name: "Lamp 19",
			channelId: 19,
			type: "basic"
		},{
			name: "Lamp 2",
			channelId: 2,
			type: "basic"
		},{
			name: "Lamp 5",
			channelId: 5,
			type: "basic"
		},{
			name: "Lamp 1",
			channelId: 1,
			type: "basic"
		}]
	},
	{
		name: "Page 3",
		channels: [{
			name: "Topless lamp",
			channelId: 6,
			type: "basic"
		}]
	}
];
for(var i = 1; i <= 100; i++){
	pages[0].channels.push({
		name: "Ch " + i,
		channelId: i,
		type: "basic"
	});
}

self.getSaveName = function(){
	return "ui-manager";
};

self.getSaveData = function(){
	return pages;
};

self.restoreSaveData = function(saveData){
	pages = saveData || [];
	var evtData = {};
	pages.forEach(function(page, idx){
		evtData[idx] = page;
	});
	this.emit("uipageupdate", evtData);
};

self.getPages = function(pageIds){
	if(typeof pageIds === "number"){
		if(pageIds < pages.length){
			return pages[pageIds];
		}
		return;
	}
	if(typeof pageIds === "undefined"){
		pageIds = [];
		for(var i = 0; i < pages.length; i++){
			pageIds.push(i);
		}
	}
	var ret = {};
	pageIds.forEach(function(idx){
		if(pages.hasOwnProperty(idx)){
			ret[idx] = pages[idx];
		}
	});
	return ret;
};

// Set a single page level
self.setPage = function(pageIdx, page){
	var obj = {};
	obj[pageIdx] = page;
	this.setPages(obj);
};

self.setPages = function(newPages){
	var page;
	var updateList = {};
	for(var pageIdx in newPages){
		if(newPages.hasOwnProperty(pageIdx)){
			page = newPages[pageIdx];
			pages[Number(pageIdx)] = page;
			updateList[pageIdx] = page;
		}
	}
	if(Object.keys(updateList).length){
		this.emit("uipageupdate", updateList);
	}
};
