"use strict";

var events = require("events");
var uuid = require("node-uuid");
var animMgr = require("./animation-manager");
var transMgr = require("./transition-manager");

var self;
module.exports = self = new events.EventEmitter();

// Animations
/* A running animation has:
 - A UUID (referencing the animation ID)
 - A UUID (referring to this running animation instance)
 - A base channel index
 - A remaining cycle count (> 0 => Actual, -1 until scene transition, -2 until cancelled)
Other fields relating to internal state will be stored separately indexed by the animation instance UUID
*/

//Public
var runningAnimations = {};
//Private
var animationState = {};

var advanceAnimation = function(animId){
	var state = animationState[animId];
	var anim = runningAnimations[animId];
	if(!state || !anim){
		return;
	}
	//advance keyframes, cycle round to the beginning if necessary, and abort if we're done
	if(state.nextKeyFrame >= state.animation.keyFrames.length){
		state.nextKeyFrame = 0;
		if(anim.remainingCycles > 0){
			anim.remainingCycles--;
			if(anim.remainingCycles == 0){
				self.cancelAnimation(animId);
				return;
			}else{
				var updateObj = {};
				updateObj[animId] = anim;
				self.emit("animrunupdate", updateObj);
			}
		}
	}
	var keyFrame = state.animation.keyFrames[state.nextKeyFrame];

	//Activate
	for(var channelIdx in keyFrame.enabledChannels){
		/* jshint eqeqeq: false */
		if(keyFrame.enabledChannels.hasOwnProperty(channelIdx) && keyFrame.channels.hasOwnProperty(channelIdx) &&
				keyFrame.enabledChannels[channelIdx] && Number(channelIdx) == channelIdx){
			/* jshint eqeqeq: true */
			transMgr.addTransition({
				channel: Number(channelIdx) + anim.baseChannel,
				duration: Number(keyFrame.transitionTime) || 0,
				level: keyFrame.channels[channelIdx],
				easing: keyFrame.easing
			});
		}
	}

	state.nextKeyFrame++;
	var advanceFunc = function(){advanceAnimation(animId);};
	if(keyFrame.waitTime == 0){
		setImmediate(advanceFunc);
	}else{
		state.nextTimeout = setTimeout(advanceFunc, keyFrame.waitTime);
	}
};

self.getAnimations = function(animIds){
	if(typeof animIds === "string"){
		if(animations.hasOwnProperty(animIds)){
			return runningAnimations[animIds];
		}
		return;
	}
	if(typeof animIds === "undefined"){
		return runningAnimations;
	}
	var ret = {};
	animIds.forEach(function(idx){
		if(animations.hasOwnProperty(idx)){
			ret[idx] = runningAnimations[idx];
		}
	});
	return ret;
};

self.startAnimation = function(animId, baseChannel, cycles){
	var animCfg = animMgr.getAnimations(animId);
	if(!animCfg){
		return;
	}

	//Clear up any channel conflicts
	var newStart = baseChannel;
	var newEnd = baseChannel + (animCfg.numChannels - 1);
	for(var otherAnimIdx in runningAnimations){
		if(runningAnimations.hasOwnProperty(otherAnimIdx)){
			var otherAnim = runningAnimations[otherAnimIdx];
			var otherStart = otherAnim.baseChannel;
			var otherEnd = otherStart + (animationState[otherAnimIdx].animation.numChannels - 1);
			if(newStart <= otherEnd && newEnd >= otherStart){
				self.cancelAnimation(otherAnimIdx);
			}
		}
	}

	var runId = uuid.v4();
	var runningAnim = {
		id: runId,
		animationId: animId,
		remainingCycles: cycles,
		baseChannel: baseChannel
	};
	var state = {
		id: runId,
		animation: animCfg,
		nextKeyFrame: 0,
		nextTimeout: null
	};
	runningAnimations[runId] = runningAnim;
	animationState[runId] = state;
	var updateObj = {};
	updateObj[runId] = runningAnim;
	self.emit("animrunupdate", updateObj);

	advanceAnimation(runId);
	return runId;
};

self.cancelAnimation = function(animInstId){
	if(runningAnimations.hasOwnProperty(animInstId)){
		var state = animationState[animInstId];
		if(state && state.nextTimeout){
			clearTimeout(state.nextTimeout);
		}
		delete runningAnimations[animInstId];
		delete animationState[animInstId];
		var updateObj = {};
		updateObj[animInstId] = null;
		self.emit("animrunupdate", updateObj);
	}
};

var getExistingAnim = function(animationId, baseChannel){
	for(var runId in runningAnimations){
		if(runningAnimations.hasOwnProperty(runId) && runningAnimations[runId].animationId === animationId && runningAnimations[runId].baseChannel === baseChannel){
			return runningAnimations[runId];
		}
	}
	return null;
};

self.sceneTransition = function(newAnims){
	var thisSceneIds = [];
	newAnims.forEach(function(anim){
		var existingAnim = getExistingAnim(anim.id, anim.baseChannel);
		if(existingAnim){
			existingAnim.remainingCycles = anim.cycles;
			thisSceneIds.push(existingAnim.id);
		}else{
			var runId = self.startAnimation(anim.id, anim.baseChannel, anim.cycles);
			thisSceneIds.push(runId);
		}
	});
	for(var runId in runningAnimations){
		if(runningAnimations.hasOwnProperty(runId) && runningAnimations[runId].remainingCycles === -1 && thisSceneIds.indexOf(runId) === -1){
			self.cancelAnimation(runId);
		}
	}
};
