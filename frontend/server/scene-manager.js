"use strict";

var events = require("events");
var transMgr = require("./transition-manager");
var animRunner = require("./animation-runner");

var self;
module.exports = self = new events.EventEmitter();

// Scenes
/* A scene is four things:
* - a name
* - a list of scene/level pairs
* - a transition time
* - an easing
*/

var scenes = [];

var activeScene = -1;

self.getSaveName = function(){
	return "scene-manager";
};

self.getSaveData = function(){
	return scenes;
};

self.restoreSaveData = function(saveData){
	scenes = saveData || [];
	var evtData = {};
	scenes.forEach(function(scene, idx){
		evtData[idx] = scene;
	});
	this.emit("sceneupdate", evtData);
};

self.getScenes = function(sceneIds){
	if(typeof sceneIds === "number"){
		if(sceneIds < scenes.length){
			return scenes[sceneIds];
		}
		return;
	}
	if(typeof sceneIds === "undefined"){
		sceneIds = [];
		for(var i = 0; i < scenes.length; i++){
			sceneIds.push(i);
		}
	}
	var ret = {};
	sceneIds.forEach(function(idx){
		if(scenes.hasOwnProperty(idx)){
			ret[idx] = scenes[idx];
		}
	});
	return ret;
};

// Set a single scene level
self.setScene = function(sceneIdx, scene){
	var obj = {};
	obj[sceneIdx] = scene;
	this.setScenes(obj);
};

//Sets multiple scene levels
self.setScenes = function(newScenes){
	var scene;
	var updateList = {};
	for(var sceneIdx in newScenes){
		if(newScenes.hasOwnProperty(sceneIdx)){
			scene = newScenes[sceneIdx];
			scenes[Number(sceneIdx)] = scene;
			updateList[sceneIdx] = scene;
		}
	}
	if(Object.keys(updateList).length){
		this.emit("sceneupdate", updateList);
	}
};

self.getActiveScene = function(){ return activeScene; };

self.activateScene = function(sceneIdx){
	var scene = scenes[sceneIdx];
	if(scene && scene.hasOwnProperty("enabledChannels") && scene.hasOwnProperty("channels")){
		var chan;
		for(var channelIdx in scene.enabledChannels){
			/* jshint eqeqeq: false */
			if(scene.enabledChannels.hasOwnProperty(channelIdx) && scene.channels.hasOwnProperty(channelIdx) &&
					scene.enabledChannels[channelIdx] && Number(channelIdx) == channelIdx){
				/* jshint eqeqeq: true */
				chan = Number(channelIdx);
				transMgr.addTransition({
					channel: chan,
					duration: Number(scene.transitionTime) || 0,
					level: scene.channels[channelIdx],
					easing: scene.easing
				});
			}
		}
		animRunner.sceneTransition(scene.animations || []);
		activeScene = sceneIdx;
		this.emit("sceneactivate", sceneIdx);
	}
};
