"use strict";

var eases = require("eases");

var self;
module.exports = self = {};

var levelmgr = require("./level-manager.js");
var timerHandle = null;
var activeCount = 0;
var activeTransitions = [];
var completedChannels = [];

var tickFunc = function(){
	var timeNow = new Date().getTime();

	var channel, channelId, channelVal;
	for(channelId in activeTransitions){
		if(activeTransitions.hasOwnProperty(channelId)){
			channel = activeTransitions[channelId];
			if(timeNow >= (channel.duration + channel.startTime)){
				channelVal = channel.toLevel;
			}else{
				channelVal = 
				Math.round(eases[channel.easing]((timeNow - channel.startTime) / channel.duration) * (channel.toLevel - channel.fromLevel)) + channel.fromLevel;
			}
			levelmgr.setLevel(channelId, channelVal);
			if(channelVal === channel.toLevel){
				completedChannels.push(channelId);
			}
		}
	}

	while(completedChannels.length){
		channelId = completedChannels.pop();
		channel = activeTransitions[channelId];
		delete activeTransitions[channelId];
		activeCount--;
		if(channel.hasOwnProperty("callback") && typeof(channel.callback) === "function"){
			try{
				channel.callback(true);
			}catch(e){}
		}
	}
	if(!activeCount){
		clearInterval(timerHandle);
		timerHandle = null;
	}
};

/*
* Implementation note - callback will receive an argument, 
*  true if the transition ran to completion, false otherwise
* Callback is guaranteed to be called asynchronously, but not necessarily next tick
*/
self.addTransition = function(config){
	var newConfig = {
		channel: config.channel,
		duration: config.duration || 0,
		toLevel: config.level,
		fromLevel: levelmgr.getLevels(config.channel),
		callback: config.callback,
		easing: config.easing,
		startTime: new Date().getTime()
	};

	if(!eases.hasOwnProperty(newConfig.easing)){
		newConfig.easing = "linear";
	}

	var oldChn = activeTransitions[newConfig.channel];
	if(oldChn){
		//To correct when count is incremented below
		activeCount--;
		if(oldChn.hasOwnProperty("callback") && typeof(oldChn.callback) === "function"){
			oldChn.callback(false);
		}
	}
	activeTransitions[newConfig.channel] = newConfig;
	activeCount++;
	if(!timerHandle){
		timerHandle = setInterval(tickFunc);
	}
};