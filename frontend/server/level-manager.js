"use strict";

var events = require("events");

var self;
module.exports = self = new events.EventEmitter();

var NUM_DMX_CHANNELS = 512;

var levels = new Buffer(NUM_DMX_CHANNELS + 1);
levels.fill(0);
//Sliced view of levels starting at idx 1, this is the view we want for OLA
var levelsView = levels.slice(1);

//Events emitted by this module:
// channelupdate - contains a dictionary of channel indexes and levels which have changed

// Set a single channel level
self.setLevel = function(channel, level){
	var obj = {};
	obj[channel] = level;
	this.setLevels(obj);
};

//Sets multiple channel levels
self.setLevels = function(channels){
	var level;
	var levelList = {};
	for(var channel in channels){
		if(channels.hasOwnProperty(channel) && Number(channel) < NUM_DMX_CHANNELS){
			channel = Number(channel);
			level = Math.min(255, Math.max(0, channels[channel]));
			if(level !== levels.readUInt8(channel)){
				levels.writeUInt8(level, channel);
				levelList[channel] = level;
			}
		}
	}
	if(Object.keys(levelList).length){
		this.emit("channelupdate", levelList);
	}
};

//Returns the state of one or more channels
//Pass a channel index to retrieve just that level
//Pass an array of channel indexes to retrieve a dictionary of levels
//Pass nothing to retrieve a dictionary of all channel levels
self.getLevels = function(channels){
	if(typeof channels === "number"){
		if(channels > 0 && channels <= NUM_DMX_CHANNELS){
			return levels.readUInt8(channels);
		}
		return;
	}
	if(typeof channels === "undefined"){
		channels = [];
		for(var i = 1; i <= NUM_DMX_CHANNELS; i++){
			channels.push(i);
		}
	}
	var ret = {};
	channels.forEach(function(channel){
		if(levels.hasOwnProperty(channel)){
			ret[channel] = levels.readUInt8(channel);
		}
	});
	return ret;
};

/*
 * Note: this returns a buffer with values at indexes at 0-511, unlike the
 * buffer used within this file which has values at 1-512. This lets the entire
 * frontend use real DMX channel values while providing an efficient method of
 * providing a different set of values to OLA
 */
self.getRawLevels = function(){
	return levelsView;
};
