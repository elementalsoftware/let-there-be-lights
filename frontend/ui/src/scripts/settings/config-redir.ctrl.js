(function(){
	"use strict";

	angular.module("ltbl.settings").controller("ConfigRedirectController", ["$stateParams", "$state", function($stateParams, $state){
		if(typeof $stateParams.pageNum === "undefined" || $stateParams.pageNum === ""){
			$state.go("config", {pageNum: 0});
		}
	}]);

})();
