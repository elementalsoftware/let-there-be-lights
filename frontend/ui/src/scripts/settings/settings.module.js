(function(){
	"use strict";

	angular.module("ltbl.settings", ["ltbl.net", "ui.router"]).config(["$stateProvider", function($stateProvider){
		$stateProvider.state("config", {
			url: "/config/:pageNum",
			templateUrl: "partials/config.html",
			controller: "ConfigRedirectController"
		});
	}]);
}());
