(function(){
	"use strict";

	angular.module("ltbl.settings").controller("ConfigEditController", ["FaderPageService", "$stateParams",
		function(FaderPageService, $stateParams){
			var self = this;
			self.editingPage = Number($stateParams.pageNum);
			self.channels = FaderPageService.list;
			var defaultNewChannel = function(){
				return {
					name: "New Channel",
					channelId: 1,
					type: "basic",
					persistentId: (Math.random() * 100000000)
				};
			};
			self.newChannel = defaultNewChannel();
			self.addChannel = function(){
				self.channels[self.editingPage].channels.push(self.newChannel);
				self.newChannel = defaultNewChannel();
			};
			self.delChannel = function(idx){
				if(idx < self.channels[self.editingPage].channels.length){
					self.channels[self.editingPage].channels.splice(idx, 1);
				}
			};
			self.swapChannels = function(idx1, idx2){
				if(idx1 < self.channels[self.editingPage].channels.length && idx1 >= 0 && idx2 < self.channels[self.editingPage].channels.length && idx2 >= 0 && idx2 !== idx1){
					var tmp = self.channels[self.editingPage].channels[idx1];
					self.channels[self.editingPage].channels[idx1] = self.channels[self.editingPage].channels[idx2];
					self.channels[self.editingPage].channels[idx2] = tmp;
				}
			};

		}
	]);

}());
