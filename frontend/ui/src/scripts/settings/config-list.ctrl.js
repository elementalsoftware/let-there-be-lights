(function(){
	"use strict";

	angular.module("ltbl.settings").controller("ConfigListController", ["FaderPageService", "$stateParams", "$state",
		function(FaderPageService, $stateParams, $state){
			var self = this;
			self.editingPage = Number($stateParams.pageId);
			self.channels = FaderPageService.list;

			self.addNew = function(){
				self.channels.push({
					name: "New Page",
					channels: [],
					persistentId: (Math.random() * 100000000)
				});
			};
			var moveNav = function(idx1, idx2){
				if(self.editingPage === idx1){
					$state.go("config", {pageId: idx2});
					return true;
				}
				return false;
			};
			self.swapPages = function(idx1, idx2){
				if(idx1 < self.channels.length && idx1 >= 0 && idx2 < self.channels.length && idx2 >= 0 && idx2 !== idx1){
					var tmp = self.channels[idx1];
					self.channels[idx1] = self.channels[idx2];
					self.channels[idx2] = tmp;
				}
				if(!moveNav(idx1, idx2)){
					moveNav(idx2, idx1);
				}
			};
			
			self.delPage = function(idx){
				if(idx < self.channels.length){
					self.channels.splice(idx, 1);
				}
			};
		}
	]);

}());
