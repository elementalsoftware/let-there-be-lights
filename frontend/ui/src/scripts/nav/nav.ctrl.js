(function(){
	"use strict";

	angular.module("ltbl.nav").controller("NavCtrl", ["FaderPageService", "SocketService",
		function(FaderPageService, SocketService){
			var self = this;
			self.channels = FaderPageService.list;
			self.fullScreen = function(){
				document.body.webkitRequestFullscreen();
				return false;
			};
		    self.saveShow = SocketService.misc.saveShow;
		}
	]);
}());
