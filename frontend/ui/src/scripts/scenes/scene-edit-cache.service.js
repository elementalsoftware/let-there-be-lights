(function(){
	"use strict";

	angular.module("ltbl.scenes").factory("SceneManagerCacheService", function(){
		return {
			currentFaderPage: 0
		};
	});

}());
