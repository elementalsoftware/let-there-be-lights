(function(){
	"use strict";

	angular.module("ltbl.scenes", ["ltbl.net", "ui.router"]).config(["$stateProvider", function($stateProvider){
		$stateProvider.state("scenes", {
			url: "/scenes",
			templateUrl: "partials/scenes.html"
		}).state("scenes.edit", {
			url: "/:sceneId",
			templateUrl: "partials/scene-edit.html",
			controller: "SceneEditController",
			controllerAs: "sceneEditCtrl",
			resolve: {
				editingScene: ["SceneService", "$stateParams", "$state", "$timeout", function(SceneService, $stateParams, $state, $timeout){
					var sceneId = Number($stateParams.sceneId);
					return SceneService.getLoadPromise().then(function successCallback(sceneList){
						if(sceneList.hasOwnProperty(sceneId)){
							return sceneList[sceneId];
						}
						$timeout(function(){
							$state.go("scenes");
						})
						throw sceneId;
					});
				}]
			}
		});
	}]);;
}());
