(function(){
	"use strict";
	angular.module("ltbl.scenes").controller("SceneEditController", ["FaderPageService", "$stateParams", "SceneService", "$location", "SceneManagerCacheService", "Easings",
		function(FaderPageService, $stateParams, SceneService, $location, SceneManagerCacheService, Easings){
			var self = this;

			self.editingScene = Number($stateParams.sceneId);
			self.channels = FaderPageService.list;
			self.easings = Easings;

			self.scenes = SceneService.list;
			self.cache = SceneManagerCacheService;
			
			self.channelInScene = function(channelId){
				if(!self.scenes.hasOwnProperty(self.editingScene) || !self.scenes[self.editingScene].hasOwnProperty("enabledChannels")){
					return false;
				}
				return !!self.scenes[self.editingScene].enabledChannels[channelId];
			};
			self.toggleChannel = function(channelId){
				if(self.scenes.hasOwnProperty(self.editingScene) && self.scenes[self.editingScene].hasOwnProperty("enabledChannels")){
					self.scenes[self.editingScene].enabledChannels[channelId] = !self.channelInScene(channelId);
				}
			};
		}
	]);
}());
