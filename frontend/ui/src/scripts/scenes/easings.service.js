(function(){
	"use strict";

	angular.module("ltbl.scenes").constant("Easings", [
		"backInOut",
		"backIn",
		"backOut",
		"bounceInOut",
		"bounceIn",
		"bounceOut",
		"circInOut",
		"circIn",
		"circOut",
		"cubicInOut",
		"cubicIn",
		"cubicOut",
		"elasticInOut",
		"elasticIn",
		"elasticOut",
		"expoInOut",
		"expoIn",
		"expoOut",
		"linear",
		"quadInOut",
		"quadIn",
		"quadOut",
		"quartInOut",
		"quartIn",
		"quartOut",
		"quintInOut",
		"quintIn",
		"quintOut",
		"sineInOut",
		"sineIn",
		"sineOut"
	]);
}());
