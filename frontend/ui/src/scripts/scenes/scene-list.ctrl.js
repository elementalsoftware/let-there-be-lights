(function(){
	"use strict";

	angular.module("ltbl.scenes").controller("SceneListController", ["SceneService", "$stateParams", "$state", function(SceneService, $stateParams, $state){
		var self = this;
		self.scenes = SceneService.list;
		self.getCurrentScene = function(){
			return Number($stateParams.sceneId);
		};
		self.addNew = function(){
			SceneService.add();
		};
		var moveNav = function(idx1, idx2){
			if(self.getCurrentScene() === idx1){
				$state.go("scenes.edit", {sceneId: idx2});
				return true;
			}
			return false;
		};
		self.swapScenes = function(idx1, idx2){
			if(idx1 < self.scenes.length && idx1 >= 0 && idx2 < self.scenes.length && idx2 >= 0 && idx2 !== idx1){
				var tmp = self.scenes[idx1];
				self.scenes[idx1] = self.scenes[idx2];
				self.scenes[idx2] = tmp;
			}
			if(!moveNav(idx1, idx2)){
				moveNav(idx2, idx1);
			}
		};
		self.delScene = function(idx){
			if(idx < self.scenes.length){
				if(idx == self.getCurrentScene()){
					$state.go("scenes").then(function(){
						self.scenes.splice(idx, 1);
					});
				}else if(idx < self.getCurrentScene()){
					$state.go("scenes.edit", {sceneId: self.getCurrentScene() - 1}).then(function(){
						self.scenes.splice(idx, 1);
					});
				}else{
					self.scenes.splice(idx, 1);
				}
			}
		};
	}]);
}());
