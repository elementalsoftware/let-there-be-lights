(function(){
	"use strict";
	angular.module("ltbl", [
		"ngTouch",
		"ui.router",
		"ltbl.faders",
		"ltbl.scenes",
		"ltbl.settings",
		"ltbl.nav",
		"ltbl.animations"
	])
	.config(["$urlRouterProvider", function($urlRouterProvider){
		$urlRouterProvider.otherwise("/faders/0");

	}]);
}());
