(function(){
	"use strict";

	angular.module("ltbl.faders", ["ltbl.net", "ui.router", "ltbl.animations"]).config(["$stateProvider", function($stateProvider){
		$stateProvider.state("faders", {
			url: "/faders/:pageNum",
			templateUrl: "partials/faders.html",
			controller: "FaderRedirectController"
		});
	}]);
}());
