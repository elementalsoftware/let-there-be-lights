(function(){
	"use strict";

	angular.module("ltbl.faders").controller("FaderAnimController", ["AnimationService", function(AnimationService){
		var self = this;

		self.animations = AnimationService.list;
		self.runningAnimations = AnimationService.running;
		var resetNewAnim = function(){
			self.newAnim = {
				cycleSelect: -2,
				cycleCount: 3
			};
		};
		resetNewAnim();

		self.startAnimation = function(){
			var cycles = Number(self.newAnim.cycleSelect) || Number(self.newAnim.cycleCount);
			if(self.animations.hasOwnProperty(self.newAnim.animId)){
				AnimationService.startAnimation(self.newAnim.animId, self.newAnim.baseChannel, cycles);
				resetNewAnim();
			}
		};
		self.cancelAnimation = function(runId){
			if(self.runningAnimations.hasOwnProperty(runId)){
				AnimationService.cancelAnimation(runId);
			}
		};
	}]);

}());
