(function(){
	"use strict";

	angular.module("ltbl.faders").controller("FaderSceneController", ["SceneService", function(SceneService){
		var self = this;

		self.scenes = SceneService.list;
		self.createScene = SceneService.add;
		self.stage = SceneService.stage;
		self.activate = SceneService.activate;
		self.selectedScenes = SceneService.selectedScenes;
	}]);

}());
