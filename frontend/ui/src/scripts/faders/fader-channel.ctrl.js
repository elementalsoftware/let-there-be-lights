(function(){
	"use strict";

	angular.module("ltbl.faders").controller("FaderChannelController", ["ChannelLevelService", "FaderPageService", "$stateParams", "SceneService", function(ChannelLevelService, FaderPageService, $stateParams, SceneService){
			var self = this;
			var tempChannelLevels = {};
			self.currentFaderPage = $stateParams.pageNum;
			self.channelLevels = ChannelLevelService.list;
			self.channels = FaderPageService.list;

			self.flashOn = function(channelId){
				if(!tempChannelLevels.hasOwnProperty(channelId)){
					tempChannelLevels[channelId] = self.channelLevels[channelId];
					self.channelLevels[channelId] = 255;
				}
			};
			self.flashOff = function(channelId){
				if(tempChannelLevels.hasOwnProperty(channelId)){
					self.channelLevels[channelId] = tempChannelLevels[channelId];
					delete tempChannelLevels[channelId];
				}
			};


		}
	]);

})();
