(function(){
	"use strict";
	angular.module("ltbl.faders")

	.directive("fader", function () {
		return {
			restrict: "AE",
			scope: {
				min: "@",
				step: "@",
				max: "@",
				callback: "@",
				margin: "@",
				behaviour: "@",
				orientation: "@",
				connect: "@",
				direction: "@",
				ngModel: "="
			},
			link: function (scope, element /*, attrs*/) {
				var callback, parsedValue, slider, minValue, maxValue;
				slider = $(element);
				callback = scope.callback ? scope.callback : "slide";
				minValue = parseFloat(scope.min);
				maxValue = parseFloat(scope.max);

				scope.ngModel = scope.ngModel || minValue;

				parsedValue = null;
				slider.noUiSlider({
					start: scope.ngModel,
					step: parseFloat(scope.step || 1),
					animate: false,
					range: {
						min: minValue,
						max: maxValue
					},
					behaviour: scope.behaviour,
					orientation: scope.orientation,
					direction: scope.direction,
					connect: scope.connect
				});
				slider.on(callback, function () {
					parsedValue = parseFloat(slider.val());
					scope.$apply(function () {
						scope.ngModel = parsedValue;
					});
				});
				scope.$watch("ngModel", function (newVal /*, oldVal*/) {
					if (newVal !== parsedValue) {
						slider.val(newVal);
						parsedValue = parseFloat(slider.val());
					}
				});
			}
		};
	});
}());
