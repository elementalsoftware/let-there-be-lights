(function(){
	"use strict";

	angular.module("ltbl.faders").controller("FaderRedirectController", ["$stateParams", "$state", function($stateParams, $state){
		if(typeof $stateParams.pageNum === "undefined" || $stateParams.pageNum === ""){
			$state.go("faders", {pageNum: 0});
		}
	}]);

})();
