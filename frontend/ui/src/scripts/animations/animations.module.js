(function(){
	"use strict";

	angular.module("ltbl.animations", ["ltbl.net", "ui.router"]).config(["$stateProvider", function($stateProvider){
		$stateProvider.state("animations", {
			url: "/animations",
			templateUrl: "partials/animations.html"
		}).state("animations.edit", {
			url: "/:animationId",
			templateUrl: "partials/animations-edit.html",
			controller: "AnimationEditController",
			controllerAs: "animEditCtrl",
			resolve: {
				editingAnimation: ["AnimationService", "$stateParams", "$state", "$timeout", function(AnimationService, $stateParams, $state, $timeout){
					var animId = $stateParams.animationId;
					return AnimationService.getLoadPromise().then(function successCallback(animations){
						if(animations.hasOwnProperty(animId)){
							return animations[animId];
						}
						$timeout(function(){
							$state.go("animations");
						});
						throw animId;
					});
				}]
			}
		}).state("animations.edit.keyframe", {
			url: "/:keyFrameIdx",
			templateUrl: "partials/animations-edit-keyframe.html",
			controller: "AnimationKeyFrameEditController",
			controllerAs: "animKFCtrl",
			resolve: {
				editingKeyFrame: ["AnimationService", "$stateParams", "$state", "$timeout", function(AnimationService, $stateParams, $state, $timeout){
					var animId = $stateParams.animationId;
					var frameIdx = Number($stateParams.keyFrameIdx);
					return AnimationService.getLoadPromise().then(function successCallback(animations){
						if(animations.hasOwnProperty(animId)){
							if(frameIdx < animations[animId].keyFrames.length){
								return animations[animId].keyFrames[frameIdx];
							}
							$timeout(function(){
								$state.go("animations.edit", {animationId: animId});
							});
							throw frameIdx;
						}
						$timeout(function(){
							$state.go("animations");
						});
						throw frameIdx;
					});
				}]
			}
		});
	}]);
}());
