(function(){
	"use strict";

	angular.module("ltbl.animations").controller("AnimationListController", ["AnimationService", "$stateParams", "$state", function(AnimationService, $stateParams, $state){
		var self = this;
		self.animations = AnimationService.list;
		self.getCurrentAnimation = function(){
			return $stateParams.animationId;
		};
		self.getCurrentKeyFrame = function(){
			return Number($stateParams.keyFrameIdx)
		}
		self.addNew = function(){
			var newAnim = AnimationService.add();
			$state.go("animations.edit", {animationId: newAnim.id});
		};
		self.getTotalDuration = function(anim){
			return anim.keyFrames.map(function(frame){ return frame.waitTime; }).reduce(function(accum, t){ return accum + t; }, 0);
		};

		self.addKeyFrame = function(anim){
			if(self.animations.hasOwnProperty(anim) && anim === self.getCurrentAnimation()){
				self.animations[anim].keyFrames.push(AnimationService.getNewKeyFrame());
				$state.go("animations.edit.keyframe", {
					animationId: anim,
					keyFrameIdx: self.animations[anim].keyFrames.length - 1
				});
			}
		};

		self.delAnimation = function(id){
			if(self.animations.hasOwnProperty(id)){
				if(id == self.getCurrentAnimation()){
					$state.go("animations").then(function(){
						delete self.animations[id];
					});
				}else{
					delete self.animations[id];
				}
			}
		};

		self.delKeyFrame = function(anim, keyframe){
			if(self.animations.hasOwnProperty(anim) && anim === self.getCurrentAnimation()
				&& keyframe < self.animations[anim].keyFrames.length && keyframe > 0 &&
				self.animations[anim].keyFrames.length > 2){

				if(keyframe == self.getCurrentKeyFrame()){
					$state.go("animations.edit", {animationId: self.getCurrentAnimation()}).then(function(){
						self.animations[anim].keyFrames.splice(keyframe, 1);
					});
				}else if(keyframe < self.getCurrentKeyFrame()){
					$state.go("animations.edit.keyframe", {
						animationId: self.getCurrentAnimation(),
						keyFrameIdx: self.getCurrentKeyFrame() - 1
					}).then(function(){
						self.animations[anim].keyFrames.splice(keyframe, 1);
					});
				}else{
					self.animations[anim].keyFrames.splice(keyframe, 1);
				}


			}
		};
	}]);
}());
