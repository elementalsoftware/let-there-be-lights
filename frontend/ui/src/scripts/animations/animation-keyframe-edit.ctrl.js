(function(){
	"use strict";
	angular.module("ltbl.animations").controller("AnimationKeyFrameEditController", ["AnimationService", "$stateParams", "Easings",
		function(AnimationService, $stateParams, Easings){
			var self = this;

			self.editingAnimation = $stateParams.animationId;
			self.editingKeyFrame = Number($stateParams.keyFrameIdx)

			self.animations = AnimationService.list;
			self.easings = Easings;

			var validFrame = function(){
				return self.animations.hasOwnProperty(self.editingAnimation) && self.editingKeyFrame < self.animations[self.editingAnimation].keyFrames.length
					&& self.animations[self.editingAnimation].keyFrames[self.editingKeyFrame].hasOwnProperty("enabledChannels");
			}

			self.channelEnabled = function(channelId){
				if(validFrame()){
					return !!self.animations[self.editingAnimation].keyFrames[self.editingKeyFrame].enabledChannels[channelId];
				}
				return false;
			};
			self.toggleChannel = function(channelId){
				if(validFrame()){
					self.animations[self.editingAnimation].keyFrames[self.editingKeyFrame].enabledChannels[channelId] = !self.channelEnabled(channelId);
				}
			};

		}
	]);
}());
