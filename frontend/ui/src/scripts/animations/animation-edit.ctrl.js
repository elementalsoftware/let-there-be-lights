(function(){
	"use strict";
	angular.module("ltbl.animations").controller("AnimationEditController", ["AnimationService", "$stateParams",
		function(AnimationService, $stateParams){
			var self = this;

			self.editingAnimation = $stateParams.animationId;

			self.animations = AnimationService.list;

			var getAnimation = function(){
				return self.animations[self.editingAnimation];
			};

			var getNextChannelId = function(){
				//TODO: Fix when we have variable-length faders
				return self.animations[self.editingAnimation].faderConfig.length;
			};

			var genNewFader = function(){
				var chan = AnimationService.getNewDefaultFader();
				chan.channelId = getNextChannelId();
				chan.name = "Ch " + (chan.channelId + 1);
				return chan;
			};
			self.newChannel = genNewFader();

			self.addChannel = function(){
				var anim = getAnimation();
				anim.faderConfig.push(self.newChannel);
				anim.numChannels++;
				self.newChannel = genNewFader();
			};
			self.delChannel = function(idx){
				var anim = getAnimation();
				if(idx < anim.faderConfig.length){
					anim.faderConfig.splice(idx, 1);
					for(var i = idx; i < anim.faderConfig.length; i++){
						anim.faderConfig[i].channelId -= 1;
						//TODO: Move channel values in keyframes?
					}
					self.newChannel.channelId -= 1;
					anim.numChannels--;
				}
			};
		}
	]);
}());
