(function(){
	"use strict";

	angular.module("ltbl.net").factory("AnimationService", ["SIOBindService", "SocketService", "UUID", function(SIOBindService, SocketService, UUID){
		var socket = SocketService.socket;
		var animations = {};
		var runningAnimations = {};

		var getNewKeyFrame = function(){
			return {
				transitionTime: 0,
				waitTime: 1000,
				easing: "linear",
				channels: {},
				enabledChannels: {}
			};
		}

		/*
		 * Important note: Animation channel IDs start at 0, they are then
		 * computed as baseChannelId (1-512) + animation channelId (0-...).
		 * For consistency purposes we label animation channel IDs
		 * as 1-... in the UI however
		 */
		var getNewDefaultFader = function(){
			return {
				name: "Ch 1",
				channelId: 0,
				type: "basic"
			};
		};

		var add = function(){
			var id = UUID.v4();

			var newAnim = {
				id: id,
				name: "New Animation",
				numChannels: 1,
				faderConfig: [getNewDefaultFader()],
				keyFrames: [getNewKeyFrame(), getNewKeyFrame()]
			};

			animations[id] = newAnim;
			return newAnim;
		};

		var startAnimation = function(animId, baseChannel, cycles){
			socket.emit("activateanim", animId, baseChannel, cycles);
		};
		var cancelAnimation = function(runId){
			socket.emit("cancelanim", runId);
		};

		var loadPromise = SIOBindService.bind(animations, "anim");
		SIOBindService.bind(runningAnimations, "animrun");

		return {
			list: animations,
			running: runningAnimations,
			add: add,
			getNewKeyFrame: getNewKeyFrame,
			getNewDefaultFader: getNewDefaultFader,
			startAnimation: startAnimation,
			cancelAnimation: cancelAnimation,
			getLoadPromise: function(){ return loadPromise; }
		};
	}]);
}());
