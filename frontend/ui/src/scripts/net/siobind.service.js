(function(){
	"use strict";

	angular.module("ltbl.net").factory("SIOBindService", ["$rootScope", "SocketService", "$q", function($rootScope, SocketService, $q){
		return {
			bind: function(dataObj, updateType){
				return $q(function(resolve){
					var firstLoad = true;
					//To prevent client notifying server of change to channel we just got told had changed
					var updatedKeys = [];
					var isArray = angular.isArray(dataObj);
					SocketService.subscribe[updateType](function(updateObj){
						$rootScope.$apply(function(){
							for(var idx in updateObj){
								if(updateObj.hasOwnProperty(idx)){
									if(isArray){
										if(updateObj[idx] === null){
											dataObj.splice(Number(idx), 1);
										}else{
											dataObj[Number(idx)] = updateObj[idx];
										}
									}else{
										if(updateObj[idx] === null){
											delete dataObj[idx];
										}else{
											dataObj[idx] = updateObj[idx];
										}
									}
									updatedKeys.push(idx);
								}
							}
						});
						updatedKeys = [];
						if(firstLoad){
							firstLoad = false;
							resolve(dataObj);
						}
					});
					$rootScope.$watch(function(){
						return dataObj;
					}, function(newValue, oldValue){
						var updateObj = {};
						var checkedKeys = [];
						Object.keys(newValue).forEach(function(objId){
							var obj = newValue[objId];
							checkedKeys.push(obj);
							if(!angular.equals(obj, oldValue[objId]) && updatedKeys.indexOf(objId) === -1){
								updateObj[objId] = obj || null;
							}
						});
						if(oldValue){
							Object.keys(oldValue).forEach(function(objId){
								var obj = oldValue[objId];
								if(checkedKeys.indexOf(obj) === -1){
									if(!angular.equals(obj, newValue[objId]) && updatedKeys.indexOf(objId) === -1){
										updateObj[objId] = newValue[objId] || null;
									}
								}
							});
						}
						if(Object.keys(updateObj).length){
							SocketService.update[updateType](updateObj);
						}
					}, true);
				});
			}
		};
	}]);
})
