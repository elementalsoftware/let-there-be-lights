(function(){
	"use strict";
	angular.module("ltbl.net").factory("ChannelLevelService", ["SIOBindService", function(SIOBindService){
		var channels = [];
		for(var i = 0; i < 512; i++){
			channels.push(0);
		}

		SIOBindService.bind(channels, "channel");

		return {
			list: channels
		};
	}]);
}());
