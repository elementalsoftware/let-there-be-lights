(function(){
	"use strict";

	angular.module("ltbl.net").factory("SceneService", ["SIOBindService", "SocketService", "UUID", function(SIOBindService, SocketService, UUID){
		var socket = SocketService.socket;
		var scenes = [];
		var selectedScenes = {active: -1, staged: 0};
		var add = function(){
			scenes.push({
				name: "New Scene",
				transitionTime: 0,
				easing: "linear",
				channels: {},
				enabledChannels: {},
				animations: [],
				id: UUID.v4()
			});
		};
		var stageForActivaion = function(sceneID){
			selectedScenes.staged = sceneID;
		};
		var activate = function(){
			selectedScenes.active = selectedScenes.staged;
			socket.emit("activatescene", selectedScenes.active);
		};
		var loadPromise = SIOBindService.bind(scenes, "scene");
		socket.on("sceneactivate", function(sceneIndex){
			selectedScenes.active = sceneIndex;
			selectedScenes.staged = sceneIndex+1;
		});
		return {
			list: scenes,
			add: add,
			stage: stageForActivaion,
			activate: activate,
			selectedScenes: selectedScenes,
			getLoadPromise: function(){ return loadPromise; }
		};
	}]);
}());
