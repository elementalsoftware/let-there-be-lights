(function(){
	"use strict";

	angular.module("ltbl.net").factory("FaderPageService", ["SIOBindService", function(SIOBindService){
		var pages = [];

		SIOBindService.bind(pages, "uipage");

		return {
			list: pages
		};
	}]);
}());
