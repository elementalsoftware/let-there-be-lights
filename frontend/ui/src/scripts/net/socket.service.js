(function(){
	"use strict";

	angular.module("ltbl.net").factory("SocketService", [function(){
		/* global io:true */
		var socket = io.connect();
		var eventTypes = ["channel", "uipage", "scene", "anim", "animrun"];
		var eventListeners = {};
		var service = {
			socket: socket,
			subscribe: {},
			unsubscribe: {},
			update: {},
			misc: {
				saveShow: function(){
					socket.emit("saveshow");
				}
			}
		};
		var connected = false;
		socket.on("connect", function(){
			connected = true;
		});
		eventTypes.forEach(function(evt){
			eventListeners[evt] = [];
			service.subscribe[evt] = function(cb){
				eventListeners[evt].push(cb);
				var fetchFunc = function(){
					socket.emit("request"+evt+"update");
				};
				if(connected){
					fetchFunc();
				}else{
					socket.on("connect", fetchFunc);
				}
			};
			service.unsubscribe[evt] = function(cb){
				var idx = eventListeners[evt].indexOf(cb);
				if(idx !== -1){
					eventListeners[evt].splice(idx, 1);
				}
			};
			service.update[evt] = function(){
				var args = Array.prototype.slice.call(arguments);
				args.unshift(evt+"update");
				socket.emit.apply(socket, args);
			};
			socket.on(evt+"update", function(){
				var argList = Array.prototype.slice.call(arguments);
				eventListeners[evt].forEach(function(cb){
					cb.apply(null, argList);
				});
			});
		});

		return service;
	}]);

}());
