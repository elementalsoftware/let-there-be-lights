"use strict";

//Must create different instance of mgr each time to prevent bleed-over
var basePath = "../../frontend/server/";
var levelMgrPath = basePath + "level-manager.js";
var transMgrPath = basePath + "transition-manager.js";

module.exports = {
	testBasicTransition: function(test){
		test.expect(3);
		var lmgr = require(levelMgrPath);
		var tmgr = require(transMgrPath);

		lmgr.setLevel(5, 0);
		tmgr.addTransition({channel: 5, level: 255, duration: 500});

		test.equal(lmgr.getLevels(5), 0);
		setTimeout(function(){
			var lvl = lmgr.getLevels(5);
			test.ok(lvl >= 100 && lvl <= 150);
		}, 250);
		setTimeout(function(){
			test.equal(lmgr.getLevels(5), 255);
			test.done();
		}, 520);
	},
	testNegativeTransition: function(test){
		test.expect(3);
		var lmgr = require(levelMgrPath);
		var tmgr = require(transMgrPath);

		lmgr.setLevel(6, 200);
		tmgr.addTransition({channel: 6, level: 0, duration: 500});

		test.equal(lmgr.getLevels(6), 200);
		setTimeout(function(){
			var lvl = lmgr.getLevels(6);
			test.ok(lvl >= 90 && lvl <= 110);
		}, 250);
		setTimeout(function(){
			test.equal(lmgr.getLevels(6), 0);
			test.done();
		}, 520);
	},
	testCallback: function(test){
		test.expect(2);
		var lmgr = require(levelMgrPath);
		var tmgr = require(transMgrPath);

		lmgr.setLevel(7, 150);
		tmgr.addTransition({channel: 7, level: 200, duration: 200, callback: function(completed){
			test.equal(completed, true);
			test.equal(lmgr.getLevels(7), 200);
			test.done();
		}});
	},
	testMultipleTransitions: function(test){
		test.expect(9);
		var lmgr = require(levelMgrPath);
		var tmgr = require(transMgrPath);

		lmgr.setLevel(50, 0);
		lmgr.setLevel(51, 0);
		lmgr.setLevel(52, 50);
		lmgr.setLevel(53, 100);
		tmgr.addTransition({channel: 50, level: 200, duration: 200});
		tmgr.addTransition({channel: 51, level: 200, duration: 500});
		tmgr.addTransition({channel: 52, level: 200, duration: 500});
		tmgr.addTransition({channel: 53, level: 200, duration: 500, callback: function(){
			setTimeout(function(){
				test.equal(lmgr.getLevels(51), 200);
				test.equal(lmgr.getLevels(52), 200);
				test.equal(lmgr.getLevels(53), 200);
				test.done();
			}, 20);
		}});
		setTimeout(function(){
			test.equal(lmgr.getLevels(50), 200);
			test.ok(lmgr.getLevels(51) > 50 && lmgr.getLevels(51) < 150);
			test.ok(lmgr.getLevels(52) > 100 && lmgr.getLevels(51) < 160);
			test.ok(lmgr.getLevels(53) > 140 && lmgr.getLevels(51) < 175);
			test.ok(lmgr.getLevels(51) < lmgr.getLevels(52));
			test.ok(lmgr.getLevels(52) < lmgr.getLevels(53));
		}, 250);
	},
	testInterruptedTransition: function(test){
		test.expect(4);
		var lmgr = require(levelMgrPath);
		var tmgr = require(transMgrPath);

		lmgr.setLevel(9, 100);
		tmgr.addTransition({channel: 9, level: 200, duration: 400, callback: function(completed){
			test.equal(completed, false);
			test.notEqual(lmgr.getLevels(9), 200);
		}});
		setTimeout(function(){
			tmgr.addTransition({channel: 9, level: 200, duration: 500, callback: function(completed){
				test.equal(completed, true);
				test.equal(lmgr.getLevels(9), 200);
				test.done();
			}});
		}, 100);
		
	},
	testEasing: function(test){
		test.expect(3);
		var lmgr = require(levelMgrPath);
		var tmgr = require(transMgrPath);

		lmgr.setLevel(18, 0);
		tmgr.addTransition({channel: 18, level: 200, duration: 500, easing: "expoOut"});

		test.equal(lmgr.getLevels(18), 0);
		setTimeout(function(){
			var lvl = lmgr.getLevels(18);
			test.ok(lvl >= 180 && lvl < 200);
		}, 250);
		setTimeout(function(){
			test.equal(lmgr.getLevels(18), 200);
			test.done();
		}, 520);
	}
};