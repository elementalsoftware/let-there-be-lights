"use strict";

//Must create different instance of mgr each time to prevent bleed-over
var mgrPath = "../../frontend/server/level-manager.js";

module.exports = {
	testBasicLevels: function(test){
		test.expect(1);
		var mgr = require(mgrPath);
		test.equal(mgr.getLevels(1), 0);
		test.done();
	},
	testSetLevel: function(test){
		test.expect(3);
		var mgr = require(mgrPath);
		mgr.setLevel(1, 50);
		mgr.setLevel(2, 55);
		mgr.setLevel(8, 60);
		test.equal(mgr.getLevels(1), 50);
		test.equal(mgr.getLevels(2), 55);
		test.equal(mgr.getLevels(8), 60);
		test.done();
	},
	testSetLevels: function(test){
		test.expect(3);
		var mgr = require(mgrPath);
		mgr.setLevels({1: 72, 2: 68, 10: 101});
		test.equal(mgr.getLevels(1), 72);
		test.equal(mgr.getLevels(2), 68);
		test.equal(mgr.getLevels(10), 101);
		test.done();
	},
	testGetLevelOutOfRange: function(test){
		test.expect(1);
		var mgr = require(mgrPath);
		test.equal(typeof(mgr.getLevels(514)), "undefined");
		test.done();
	},
	testGetLevels: function(test){
		test.expect(3);
		var mgr = require(mgrPath);
		mgr.setLevels({1: 86, 2: 42, 200: 44});
		var y = mgr.getLevels([1,2,200]);
		test.equal(y[1], 86);
		test.equal(y[2], 42);
		test.equal(y[200], 44);
		test.done();
	},
	testEventSingle: function(test){
		test.expect(2);
		var mgr = require(mgrPath);
		mgr.on("channelupdate", function(){
			test.ok(true, "Test Event Fired");
		});
		mgr.setLevel(42, 12);
		mgr.setLevel(42, 13);

		//Should not fire an event
		mgr.setLevel(42, 13);

		test.done();
	},
	testEventMultiple: function(test){
		test.expect(6);
		var mgr = require(mgrPath);
		mgr.on("channelupdate", function(levelList){
			test.equal(Object.keys(levelList).length, 2);
			test.ok(levelList[48] >= 155 && levelList[48] <= 156);
			test.ok(levelList.hasOwnProperty(49) || levelList.hasOwnProperty(50));
		});
		mgr.setLevels({48: 155, 49: 122});

		//Fired event should only contain 2 keys as 49 hasn't changed
		mgr.setLevels({48: 156, 49: 122, 50: 130});

		test.done();
	}
};