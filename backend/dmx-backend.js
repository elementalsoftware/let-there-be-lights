"use strict";

var config = require("../config.js");

var self;
module.exports = self = {};

self.bind = function(frontend){
	var me = this;
	this.client = require("./OLAInterface");

	var updateOla = function(){
		me.client.UpdateDmxData(config.ola.universe, frontend.levelmgr.getRawLevels());
	};

	this.client.on("connected", function(){
		me.client.GetDmx(config.ola.universe, function(err, dmxData){
			if(err){
				console.error("Got error from OLA - ", err);
				return;
			}
			frontend.levelmgr.setLevels(dmxData);
			process.nextTick(function(){
				updateOla();
			});
		});
	});
	frontend.levelmgr.on("channelupdate", updateOla);
};
