"use strict";
/* jslint bitwise:true */
var events = require("events");
var net = require("net");

var ProtoBuf = require("protobufjs");
var rpcproto = ProtoBuf.loadProtoFile("backend/Rpc.proto").build("ola.rpc");
var olaproto = ProtoBuf.loadProtoFile("backend/Ola.proto").build("ola.proto");

var self;
module.exports = self = new events.EventEmitter();
var nextReqId = Math.floor(Math.random() * 0xFFFFFFF);

var client = net.connect(9010, "127.0.0.1");
client.on("connect", function(){
	self.emit("connected");
});

var reqsInFlight = {};
var processResponse = function(rpcmsg){
	if(!reqsInFlight.hasOwnProperty(rpcmsg.id)){
		return;
	}
	var flightNote = reqsInFlight[rpcmsg.id];
	delete reqsInFlight[rpcmsg.id];
	if(!flightNote.callback){
		return;
	}
	if(rpcmsg.type === rpcproto.Type.REQUEST_FAILED){
		var errmsg = rpcmsg.getBuffer().toString("ascii");
		flightNote.callback(errmsg);
	}else{
		var msg = flightNote.response.decode(rpcmsg.getBuffer());
		flightNote.callback(null, msg);
	}
};

var sendRequest = function(request, requestMethod, responseType, callback){
	var flightNote = {
		response: responseType,
		id: nextReqId,
		callback: callback
	};
	if(responseType !== olaproto.STREAMING_NO_RESPONSE){
		reqsInFlight[flightNote.id] = flightNote;
	}
	nextReqId++;
	var rpcmsg = new rpcproto.RpcMessage({
		type: rpcproto.Type.REQUEST,
		id: flightNote.id,
		name: requestMethod,
		buffer: request.toBuffer()
	});
	var rpcbuf = rpcmsg.toBuffer();
	var sizeBuf = new Buffer(4);
	sizeBuf.writeUInt32LE((rpcbuf.length & 0xFFFFFFF) | (1 << 28), 0);
	client.write(sizeBuf);
	client.write(rpcbuf);
};

self.UpdateDmxData = function(universe, dmxdata, callback){
	var buf;
	if(dmxdata instanceof Buffer){
		buf = dmxdata;
	}else{
		buf = new Buffer(512);
		buf.fill(0);
		for(var i = 0; i < 512; i++){
			if(dmxdata.hasOwnProperty(i)){
				buf.writeUInt8(dmxdata[i], i);
			}
		}
	}

	var req = new olaproto.DmxData({
		universe: universe,
		data: buf
	});
	if(callback){
		sendRequest(req, "UpdateDmxData", olaproto.Ack, function(err){ callback(err); });
	}else{
		sendRequest(req, "StreamDmxData", olaproto.STREAMING_NO_RESPONSE);
	}
};

self.GetDmx = function(universe, callback){
	var req = new olaproto.UniverseRequest({
		universe: universe
	});
	sendRequest(req, "GetDmx", olaproto.DmxData, function(err, data){
		if(!callback){
			return;
		}
		if(err){
			return callback(err);
		}
		var data2 = {};
		var buf = data.getData().toBuffer();
		for(var i = 0; i < buf.length; i++){
			data2[i] = buf.readUInt8(i);
		}
		callback(null, data2);
	});
};

var nextSize = -1;
var partialBuffers = [];
var handleNetworkMessage = function(data){
	if(nextSize === -1){
		nextSize = data.readUInt32LE(0) & 0xFFFFFFF;
		data = data.slice(4);
	}

	if(partialBuffers.length){
		partialBuffers.push(data);
		var totalSize = 0;
		partialBuffers.forEach(function(buf){
			totalSize += buf.length;
		});
		if(totalSize < nextSize){
			return;
		}
		data = Buffer.concat(partialBuffers, totalSize);
		partialBuffers = [];
	}else{
		if(data.length < nextSize){
			partialBuffers.push(data);
			return;
		}
	}
	
	if(data.length > nextSize){
		partialBuffers.push(data.slice(nextSize));
		data = data.slice(0, nextSize);
	}

	nextSize = -1;

	var rpcmsg = rpcproto.RpcMessage.decode(data);
	if(rpcmsg.type === rpcproto.Type.RESPONSE || rpcmsg.type === rpcproto.Type.RESPONSE_FAILED){
		processResponse(rpcmsg);
	}else{
		console.log("Received RPC message with type ", rpcmsg.type);
	}
	if(partialBuffers.length){
		handleNetworkMessage(partialBuffers.shift());
	}
};
client.on("data", handleNetworkMessage);

