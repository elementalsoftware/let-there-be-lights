#!/bin/bash

### BEGIN INIT INFO
# Provides:				ltbl
# Required-Start:		$all
# Required-Stop:		$all
# Default-Start:		2 3 4 5
# Default-Stop:			0 1 6
# Short-Description:	LTBL Daemon
# Description:			Let there be light DMX Controller
### END INIT INFO
. /lib/lsb/init-functions

USER="stewart"

DAEMON="/usr/local/bin/node"
ROOT_DIR="/home/$USER/ltbl"

SERVER="$ROOT_DIR/index.js"
LOG_FILE="$ROOT_DIR/app.log"

NAME=ltbl
DESC="LTBL Server Daemon"
PIDFILE=/var/run/$NAME.pid

[ -x "$DAEMON" ] || exit 0

case "$1" in
	start)
		# master switch
		log_daemon_msg "Starting $DESC" "$NAME"
		/sbin/start-stop-daemon --start --background --make-pidfile --pidfile $PIDFILE --chdir $ROOT_DIR --chuid $USER --startas /bin/bash -- -c "exec $DAEMON $SERVER >> $LOG_FILE 2>&1"
		log_end_msg $?
		;;
	stop)
		# master switch
		log_daemon_msg "Stopping $DESC" "$NAME"
		/sbin/start-stop-daemon --stop --pidfile $PIDFILE --chuid $USER --exec $DAEMON --retry 10
		/bin/rm -f $PIDFILE
		log_end_msg $?
		;;
	reload|restart)
		$0 stop && $0 start
		;;
	status)
		status_of_proc -p $PIDFILE $DAEMON $NAME && exit 0 || exit $?
		;;
	*)
		echo "Usage: /etc/init.d/$NAME {start|stop|restart|status}" >&2
		exit 1
		;;
esac

exit 0
