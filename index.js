"use strict";

var express = require("express");
var app = express();
var server = require("http").Server(app);
var io = require("socket.io")(server);

var appConfig = require(__dirname + '/config.js');

app.use(express.static(appConfig.webserver.staticDir));


var frontend = require("./frontend/server/dmx-frontend");
frontend.socketio.bind(io);

if(appConfig.ola.enabled){
	var backend = require("./backend/dmx-backend");
	backend.bind(frontend);
}

server.listen(appConfig.webserver.port);

